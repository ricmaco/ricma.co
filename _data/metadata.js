module.exports = {
    title: "Hi, I'm Riccardo",
    displayTitle: 'Hi, I\'m <span class="handwriting">Riccardo</span>',
    description: "Riccardo Macoratti personal blog",
    keywords: [
        "blog",
        "portfolio",
        "developer",
        "personal",
        "technology",
        "software",
        "linux",
        "youtube",
    ],
    url: "https://ricma.co",
    language: "en",
    startYear: 2014,
    currentYear: new Date().getFullYear(),
};