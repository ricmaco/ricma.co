module.exports = {
    'default-src': [
        "'self'",
        "'unsafe-inline'",
        "'unsafe-eval'",
        'https://fonts.googleapis.com/',
        'https://fonts.gstatic.com/',
        'https://unpkg.com/prismjs/themes/prism-tomorrow.min.css',
    ],
};
