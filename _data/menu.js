module.exports = [
    {
        label: "whoami",
        url: "/whoami",
    },
    {
        label: "blog",
        url: "/posts",
    },
    {
        label: "cv-eng",
        url: "https://resume.io/r/UPDAbtwe0",
    },
    {
        label: "cv-ita",
        url: "https://resume.io/r/xPXherPB1",
    },
    {
        label: "projects",
        url: "/projects",
    },
    {
        label: "nixRIOT",
        url: "https://nixriot.wordpress.com",
    },
];