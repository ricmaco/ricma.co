module.exports = [
  {
    name: "Email",
    icon: "send",
    url: "mailto:r.macoratti@gmx.co.uk",
  },
  {
    name: "LinkedIn",
    icon: "linkedin",
    url: "https://www.linkedin.com/in/ricmaco/",
  },
  {
    name: "Gitlab",
    icon: "gitlab",
    url: "https://gitlab.com/ricmaco/",
  },
  {
    name: "YouTube",
    icon: "youtube",
    url: "https://www.youtube.com/c/RichardMaco00",
  },
  {
    name: "Instagram",
    icon: "instagram",
    url: "https://www.instagram.com/_ricmaco",
  },
  {
    name: "Facebook",
    icon: "facebook",
    url: "https://www.facebook.com/ricmaco",
  },
  {
    name: "RSS",
    icon: "rss",
    url: "feed/feed.xml",
  },
];
