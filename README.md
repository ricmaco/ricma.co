[![Netlify Status](https://api.netlify.com/api/v1/badges/72dc28db-3d71-4d9b-a4ef-42bbbfbe92be/deploy-status)](https://app.netlify.com/sites/ricmaco/deploys)

# Ricma.co

Source code for the [ricma.co](https://ricma.co) site.

Project is built using the excellent [Eleventy](https://www.11ty.dev/).

## Setup

The setup is like any normal Javascript application:

```bash
npm install
```

## Develop

To have a dev server on port 8080, watching for your changes:

```bash
npm start
```

Which in turn launches:

```bash
npx @11ty/eleventy --serve
```

## Build

To build a production ready package:

```bash
npm run build
```

Which in turn launches:

```bash
npx @11ty/eleventy
```

## License

All right are reserved. If in need of something, write to [the author](r.macoratti@gmx.co.uk).