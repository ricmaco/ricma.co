import { Buffer } from "node:buffer";

import { EleventyHtmlBasePlugin } from "@11ty/eleventy";

import pluginRss from "@11ty/eleventy-plugin-rss";
// @ts-ignore
import pluginSyntaxHighlight from "@11ty/eleventy-plugin-syntaxhighlight";

import CleanCSS from "clean-css";
import { minify } from "terser";
import minifyHtml from "@minify-html/node";

const globals = {
  html: (html) => html
    .trim()
    .replace(/\n\s*^\s*/gm, ' ')
    .replaceAll('> <', '><'),
}

const filters = {
  isodate: (date) => date.toISOString().split('T')[0],
  humandatetime: (date) => date.toLocaleDateString('en-US', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
  }),
  humandate: (date) => date.toLocaleString('en-US', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  }),
  humantime: (date) => date.toLocaleTimeString('en-US', {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
  }),
}

const shortcodes = {
  icon: (icon, cls) => {
    if (!icon) return null;

    return globals.html(
      `<svg class="feather${cls ? ` ${cls}` : ''}">
                <use href="/img/feather-sprite.svg#${icon}" />
            </svg>`
    );
  },
  figure: (url, alt, cls, caption) => {
    if (!url) return null;

    return globals.html(
      `<figure class="${cls ?? 'center'}">
                <img src="${url}" alt="${alt}" />
                ${caption
        ? `<figcaption>${caption}</figcaption>`
        : ''
      }
            </figure>`
    );
  },
  youtube: (id) => {
    if (!id) return null;

    return globals.html(
      `<div class="youtube">
                <iframe src="https://www.youtube-nocookie.com/embed/${id}"
                    title="YouTube video player"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    allowfullscreen>
                </iframe>
            </div>`
    );
  },
  pdf: (url) => {
    if (!url) return null;

    return globals.html(
      `<div class="pdf">
                <object type="application/pdf" data="${url}?#zoom=page-width&pagemode=none">
                    <p>PDF file cannot be displayed!</p>
                </object>
            </div>`
    );
  },
  iframe: (url) => {
    if (!url) return null;

    return globals.html(
      `<div class="iframe">
                <iframe src="${url}">
                </iframe>
            </div>`
    );
  },
  journey: (content) => {
    return globals.html(
      `<ul class="journey">
                ${content}
            </ul>`
    );
  },
  journeyitem: (content, icon) => {
    return globals.html(
      `<li class="journey-item">
                <span>
                    <svg class="feather journey-icon">
                        <use href="/img/feather-sprite.svg#${icon}" />
                    </svg>
                </span>
                ${content}
            </li>`
    );
  },
  note: (content) => {
    return globals.html(
      `<div class="note">
                <div class="note-title">
                    Note
                </div>
                <div class="note-content">
                    ${content}
                </div>
            </div>`
    );
  },
}

function configureEleventy(eleventyConfig) {
  const production = process.env.ELEVENTY_RUN_MODE == 'build';

  // plugins
  eleventyConfig.addPlugin(EleventyHtmlBasePlugin);
  eleventyConfig.addPlugin(pluginRss);
  eleventyConfig.addPlugin(pluginSyntaxHighlight);

  // custom filters
  eleventyConfig.addFilter('isodate', filters.isodate);
  eleventyConfig.addFilter('humandatetime', filters.humandatetime);
  eleventyConfig.addFilter('humandate', filters.humandate);
  eleventyConfig.addFilter('humantime', filters.humantime);

  // custom shortcodes
  eleventyConfig.addShortcode('icon', shortcodes.icon);
  eleventyConfig.addShortcode('figure', shortcodes.figure);
  eleventyConfig.addShortcode('youtube', shortcodes.youtube);
  eleventyConfig.addShortcode('pdf', shortcodes.pdf);
  eleventyConfig.addShortcode('iframe', shortcodes.iframe);
  eleventyConfig.addPairedShortcode('journey', shortcodes.journey);
  eleventyConfig.addPairedShortcode('journeyitem', shortcodes.journeyitem);
  eleventyConfig.addPairedShortcode('note', shortcodes.note);

  // passthrough
  eleventyConfig.addPassthroughCopy({
    "public": "/", // site res
    "public/favicon": "/", // favicon
    "content/images": "/" // posts images
  });

  if (production) {
    // minify css
    const cleanCss = new CleanCSS({ compatibility: 2 });
    eleventyConfig.addTransform('minify-css', function (content) {
      const outputPath = this.page.outputPath;
      if (!outputPath || !outputPath.endsWith('.css')) {
        return content;
      }
      return cleanCss.minify(content).styles;
    });

    // minify js
    eleventyConfig.addTransform('minify-js', async function (content) {
      const outputPath = this.page.outputPath;
      if (!outputPath || !outputPath.endsWith('.js')) {
        return content;
      }
      return minify(content)
        .then(minified => minified.code);
    });

    // minify html
    eleventyConfig.addTransform('minify-html', function (content) {
      const outputPath = this.page.outputPath;
      if (!outputPath || !outputPath.endsWith('.html')) {
        return content;
      }
      return minifyHtml.minify(Buffer.from(content), {});
    });
  }

  // config
  return {
    htmlTemplateEngine: "njk",
    markdownTemplateEngine: "njk",
    templateFormats: [
      "html",
      "md",
      "njk",
    ],
    dir: {
      input: "content",
      includes: "../_includes",
      data: "../_data",
    },
  };
}

export default configureEleventy;
