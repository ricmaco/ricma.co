---
title: London Science Museum
date: 2025-02-15
tags: 
    - 2025
    - museum
    - science
    - london
    - moon
    - moon landing
subtitle: My first visit to London's Science Museum
---
{%- figure
    'london-science-museum-lem.webp',
    'me at london science museum in front a lem reproduction',
    'background'
%}

I recently had the chance to visit the [London Science Museum](https://www.sciencemuseum.org.uk) on February 15, 2025, and I must say, it was an absolute blast!

We were mesmerized by the incredible collection of exhibits that showcased the fascinating world of science and technology.
One of the highlights of our visit was the space age exhibit, which left us starstruck and eager to learn more about that curious history period.

But what really caught our attention was the way the museum had curated exhibits that displayed the evolution of common objects over time.
It was amazing to see how everyday items had transformed over the years, and we found ourselves getting lost in the journey.

Unfortunately, with so much to see and explore, we didn't get to visit the entire museum in one go.
Which means, we have a great excuse to go back for another round.

We did, however, manage to squeeze in a visit to the "[Power Up](https://www.sciencemuseum.org.uk/see-and-do/power)" section, which is a stepping stone for any retrogaming enthusiast.
With a vast array of consoles from different eras, we had an amazing time exploring and playing with the various devices on display.

Our visit to the London Science Museum was definitely a worth experience.
If you're a science buff, a tech enthusiast, or just someone who loves to learn and have fun, then this museum is definitely a must-visit destination.
