---
title: Battle for the Net Neutrality
date: 2017-11-23
tags: 
    - 2017
    - net neutrality
    - free
    - freedom
    - internet
subtitle: Join the battle for the net neutrality!
---

{%- figure
    'net-neutrality.webp',
    'fight for the net neutrality banner'
%}


I know, I am European and this battle should not be my battle, but let's
analyze things from another point of view.

{%- figure
    'net-neutrality-archer.webp',
    'fight for the net neutrality meme',
    'left'
%}

Internet is objectively America-centered, U.S.A. invented the Internet
and, as such, a great, perceivable part of the internet is on the hands
of the States. For this reason, Europe should take position on Net
Neutrality matter, because even if it seems a very far concern, it is
not, it will affect our lives too, the lives of the people on the other
side of the ocean as well.

{%- figure
    'net-neutrality-cat.webp',
    'fight for the net neutrality meme',
    'right'
%}

As a fellow Internet user, wether you are a kid of the manager of a
multinational society, you **have to** take you position and defend the
medium you are using.

I am not usually a person who shouts in real life, and neither I am on
the internet, but for this time I will make an exception:

**SAVE THE INTERNET, DEFEND NETWORK NEUTRALITY, DO YOUR PART\!**

Even if you don't have any influence power, you can still do you part,
informing yourself and other on the matter. Here are some link that can
get you started:

  - [Battle for the Net](https://www.battleforthenet.com/)
  - [Fight for the Future](https://www.fightforthefuture.org/)

You don't have time or you don't feel like reading something? Here are
some video content that can get you instantly informed on you last
chanche to save the Internet as you know it.

This one is only some seconds long, directly from Fight for the Future.

{% youtube 'LYq_k_1MS4I' %}

This one, instead, is from the inventor of the World Wide Web, Sir [Tim
Berners-Lee](https://en.wikipedia.org/wiki/Tim_Berners-Lee).

{% youtube '5Gh0NIQ3yd0' %}
