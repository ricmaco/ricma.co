---
title: Codemotion Milan 2017
date: 2017-11-15
tags: 
    - 2017
    - codemotion
    - code
    - open source
    - talks
    - jobs
    - programming
subtitle: Codemotion Event in Milan, 2017 edition
---
{%- figure
    'codemotion.webp',
    'codemotion logo',
    'background'
%}

Last saturday, I had the oportunity to visit this famous event which
took place in Milan, in a new location called
[BASE](https://milan2017.codemotionworld.com/location/), 6000 sqm, that
was the old offices of the Ansaldo.

This is publicized as the "greatest technical conference of Europe,
regarding software development". And, in my poor experience, it
completely was. They programmed two days of conferences and two days of
workshops. I attended only the last day of conference.

There, more than a hundred talks took place and all leader in the market
companies were there, offering a job or simply selling their products.

Among this forest of advertisemente, there were also a lot of skilled
presenters who showed the most interesting and up to date novelties of
the technology word.

The passwords were *Faas*, that is [Function as a
service](https://en.wikipedia.org/wiki/Function_as_a_service),
*serverless*, *microservices* and *containers*. Some of the content
taught also parallell and distributed computing, big and realtime data
and, ultimately, the
[SMACK](https://mesosphere.com/blog/smack-stack-new-lamp-stack/) stack.

The most interesting talk, in my opinion, was the one about
[CSS](https://en.wikipedia.org/wiki/Cascading_Style_Sheets). I always
hated CSS and it seemes like learning Arabic to me. This talk opened my
eyes on its powers and the ability to complete supersede some functions
of Javascript. This is very specific, but while I was listening to the
presenter, my mind blew up for the magic he was showing us.

The presenter was [Davide Di
Pumpo](https://twitter.com/makhbeth?lang=en) and the
[presetation](https://www.slideshare.net/DavideDiPumpo/quella-sporca-dozzina-a-cascata)
(in Italian) it is about twelve cheats in CSS.

If you want more information on the event, you can find them on their
[site](https://milan2017.codemotionworld.com/). Maybe next year we will
meet there.

And, finally, a photo of my old laptop (which is slowly becoming a
sticker plate).

{%- figure
    'codemotion-pc.webp',
    'old pc with codemotion logo'
%}