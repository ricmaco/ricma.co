---
title: Linux Day 2015
date: 2015-10-23
tags:
    - 2015
    - linuxday
    - linuxvar
    - linux
    - talks
---
{% figure
    'linuxday2015.webp',
    'linuxday logo 2015',
    'left background'
%}

This year the [LUG I usually
attend](https://www.linuxvar.it/linux-2015/) and another
neighbour LUG, [GL-Como](http://www.gl-como.it/v2015/linux-day-2015/),
organized the 2015 edition of the [Linux Day](http://www.linuxday.it) at
the [ISISS Sant'Elia](http://www.istitutosantelia.gov.it) in Cantù, near
Como.

We organized some talks and workshops, together with a questionnaire to
make students see and try what the Linux and the open source communty
is.

I helped with the [Arduino](http://www.arduino.cc) workshop and
completely designed the questionnaire about "*young people and the
Internet*", which the student have answered on an
[LTSP](http://www.ltsp.org) (refer [here](/posts/talks/ltsp)
to some LTSP slides) post in a Linux adapted laboratory.

Furter and detailed information about presented questionnaire can be
viewed [here](/posts/talks/young-people-and-snss).

Some photos are to be admired [here](/files/linuxday/2015/photos.zip) or
[here](https://www.linuxvar.it/linux-2015/).
