---
title: Linux Day 2016
date: 2016-10-29
tags:
    - 2016
    - linuxday
    - linuxvar
    - linux
    - talks
    - gimp
    - 0x10
    - free
    - arduino
---
{% figure
    'linuxday2016.webp',
    'linuxday logo 2016',
    'left'
%}

In this period of the year, the time has come for the [Linux
Day](http://www.linuxday.it) for the area of Varese, Lombardy. This
edition we, [LinuxVar](http://www.linuxvar.it4),
[LIFO](http://lifolab.org) laboratory and
[GL-Como](http://www.gl-como.it/) LUGs, have decided to make it take
place another time at the [FaberLab](http://www.faberlab.org) of
Tradate, an interesting place where anyone can transform its ideas into
a 3D printed reality.

As usual, some talks, workshop and projects took place. This year we
decided that not only the school were to partecipate at this event, but
also they were to be part of the protagonists. So some talks on Arduino
by students for students were organized.

Further information can be found directly on the dedicated [LinuxDay 2016 page](https://www.linuxvar.it/linuxday-2016/)
on the LinuxVar site.

# LinuxVar

{% figure
    'linuxvar-0x10.webp',
    'linuxvar 0x10 cake',
    'right background'
%}

This year was also the 16th anniversary of the informal foundation of the LinuxVar LUG and the 10th from the official
declaration.

# Me

I, personally, choose to build up a workshop on the GIMP, the GNU Image
Manuplator Program. Further information on the can be found
[here](https://www.gimp.org/).

A more descrptive and detailed explanation of the workshop is available
at this [page](/posts/talks/intro-gimp), complete
with slides used to show it.

Some photos of the event can be, instead, downloaded
[here](/files/linuxday/2016/photos.zip), on my file server.
