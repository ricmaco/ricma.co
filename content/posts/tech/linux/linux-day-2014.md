---
title: Linux Day 2014
date: 2014-10-30
tags:
    - 2014
    - linuxday
    - linuxvar
    - linux
    - talks
    - ltsp
---
{% figure
    'linuxday2014.webp',
    'linuxday logo 2014',
    'left background'
%}

This year too (2014) a [Linux Day](http://www.linuxday.it) edition for
the Varese area has been organized by the
[LinuxVar](https://www.linuxvar.it/linuxday-sabato-25-ottobre-2014/) and
[GL-Como](http://www.gl-como.it/) LUG and the [LIFO](http://lifolab.org)
laboratory at the [FaberLab](http://www.faberlab.org) of Tradate, an
interesting place where you can build up your ideas, make 3D and
ceramics printings and find someone who will teach you all these
goodies.

As usual, some talks, workshop and projects took place. But this year
there has been quite a substancial difference with respect to past
editions, because for the first time the fair will be targeted
exclusively to students.

# Me

Personally I showed [LTSP](http://www.ltsp.org), a server-thin client
system for schools and organizations computer laboratories. We provided
a small lab-like structure with three posts and an LTSP server.

A more descrptive and detailed explanation of this system is present at
this [page](/posts/talks/ltsp), complete with
slides used to show it.

Some photos of the event can be, instead, downloaded
[here](/files/linuxday/2014/photos.zip), on my file server.
