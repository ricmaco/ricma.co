---
title: Linux Day 2018
date: 2018-11-12
tags:
    - 2018
    - linuxday
    - linuxvar
    - linux
    - talks
    - www
    - world wide web
    - https
---
{% figure
    'linuxday2018.webp',
    'linuxday logo 2018',
    'left background'
%}

Even this year, together with some organizative difficulties, the [Linux
Day](http://www.linuxday.it) time has come. As every past few years, I
was part of the staff team and I held some talk for the people coming
by.

This year the general theme was *World Wild Web*, so I thought of doing
some presentation of what HTTPS actually is and why we should always try
to use it and search for it. That small lock that appears on every
browser is really tiny, but it does a lot of protection work on our
behalf.

The usual local associations took their place in the event organization
and hosting.
[LinuxVar](http://www.linuxvar.it/),
[LIFO](http://lifolab.org) laboratory and
[GL-Como](http://www.gl-como.it/) LUG joined their forces to bring some
content to the partecipants. For the fourth year, the fair location was
the [FaberLab](http://www.faberlab.org) room in Tradate.

Unfortunately, this year no showroom of pratical exibitions or workshops
was there, but there were six talks in the whole afternoon. So, not bad
actually.

# My own talk

The presentation slides are of course in Italian and you can view them
below.

{% pdf '/files/linuxday/2018/s-in-https.pdf' %}

You can also download the presentation slide, in [ODP](/files/linuxday/2018/s-in-https.odp) or in [PDF
format](/files/linuxday/2018/s-in-https.pdf).

# Resources

If you want to see some photos of the event, you can find them on
[GL-Como Friendica page](https://social.gl-como.it/photos/gl-como).

Also, further information are on the deciated LinuxVar [page](https://www.linuxvar.it/linuxday-sabato-27-ottobre-2018/).
