---
title: Stack Overflow network analysis
date: 2017-06-12
tags:
    - 2017
    - stack overflow
    - network analysis
    - graph
    - big data
subtitle: A simple and straightforward analysis of the Stack Overflow answer graph
---
{% figure
    'stack-overflow.webp',
    'stack overflow logo',
    'background'
%}

As we all know, a programmer always needs help from another fellow
programmer or sometimes just wants to reach easily the best
implementation of an algorithm.

For this purpose, [Stack Overflow](https://stackoverflow.com) has been
created.

Stack Overflow is part of the [Stack
Exchange](https://stackexchange.com/) network and at the time of writing
it counts 7.3 milion users, with more than 14 milions question asked and
22m of answers given ([more
here](https://stackexchange.com/sites?view=list#traffic)).

We present a simple ans standard analysis of the Stack Overflow
constructed as follows:

  - every **node** is a **user**;
  - every **edge** answers to the question *has answered to*;
  - an optional **weight** has been given if a user has answered more
    than once to another user (but it was not used during the analysis).

Below, the notebook with the complete analysis:

{% iframe '/raw/stack-overflow/notebook.html' %}

You can download it [here](/raw/stack-overflow/notebook.html).

A presentation of the result obtained can be viewed below (also [download it]
(/files/uni/stack-overflow/presentation.pdf)).

{% pdf '/files/uni/stack-overflow/presentation.pdf' %}
