---
title: A codec evaluation test
date: 2015-09-17
tags:
    - 2015
    - codec
    - video
    - evaluation
subtitle: An evaluation of 8 codec which provide similar quality results, based on user suggestions, gathered in an open test
---
In 2015, we have reached a time where several efficient codecs for video compressing are available. Every one of them has its peculiarities as well as its drawbacks. Some it is more advanced than the other, such as in the case of H.265 and some belongs to a omnipresent past standard, which unfortunately is still much used: MPEG-2.

I elaborated a small comparison from the point of view of perceived quality. I chose to evaluate subjective quality, because it is what is sensed by users. An objective, sterile evaluation would have not given the same significant results.

I chose eight codecs, such as:

-   MPEG-2
-   H.264
-   H.265
-   VP8
-   VP9
-   Theora
-   ProRes 422
-   Dirac

To accomplish the example raw video compressione, I used the open source tool [ffmpeg](http://ffmpeg.org). I produced two versions of compression: one optimized for standard quality and one which provides high quality.

Uncompressed test scenes are at a size of 1920x1080 pixels, with a framerate of 50 Hz. They are to be found on the [VQEG FTP](ftp://vqeg.its.bldrdoc.gov/HDTV/SVT_MultiFormat/1080p50_CgrLevels_SINC_FILTER_SVTdec05_/), provided by the [VQEG](http://www.its.bldrdoc.gov/vqeg/vqeg-home.aspx).

{% pdf '/files/uni/codec-evaluation/codec_evaluation-results.pdf' %}

Download the full article containing details and results of the evaluation [here](/files/uni/codec-evaluation/codec_evaluation-results.pdf).
