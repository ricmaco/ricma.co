---
title: "Chrome OS: Nerd Fonts for the terminal"
date: 2024-02-25
tags:
    - 2024
    - tutorial
    - chrome os
    - fonts
    - dev
image: chromeos-terminal.webp
---
{% figure
    image,
    'screenshot of chrome os terminal using nerd fonts'
%}

Recently I got hold of a Chrome OS device. While I usually do not sponsor Google products, I quite like this one, because although being low spec, it brings me a viable Linux environment with close to 12 hours of battery.

Since it is such an underpowered device, but I still want to grind some code, the usual VSCode setup is not an option, so I leaned towards a [Neovim](https://neovim.io/) setup, [LunarVim](https://www.lunarvim.org/) specifically.

To be able to use LunarVim a prerequisite is to use [Nerd Fonts](https://www.nerdfonts.com/), a distribution of some popular programming fonts bundled with all sort of icons. To my surprise there is no straightforward way of setting a custom font, neiter in the Chrome OS UI, nor in the Terminal application itself (although the font selection on the latter is not bad).

But... When there is a will, there is a way, they say.

## A piece of configuration tool left over and a tutorial

Searching online I discovered the old configuration interface for the Terminal app, from the times it was only an SSH app, was still around and reachable.

Open Chrome and paste the following address in the search bar: `chrome-untrusted://terminal/html/nassh_preferences_editor.html` (I can't put a link, since being *untrusted*, as the name suggests, Chrome refuses to open it). A view like the following should open:

{% figure
    'terminal-preferences.webp',
    'screenshot of chrome os terminal old preferences view',
    '',
    'Chrome OS old Terminal preferences'
%}

From this screen, setting a custom font is a breeze, since there is a field to input custom CSS.

In the "Custom CSS (inline text)" field, put the following lines to fetch [Caskaydia Cove](https://www.programmingfonts.org/#cascadia-code), the Nerd Font version of [Cascadia Cove](https://github.com/microsoft/cascadia-code):

```css
@font-face {
    font-family: "Caskaydia Cove NFM";
    src: url("https://raw.githubusercontent.com/ryanoasis/nerd-fonts/v3.1.1/patched-fonts/CascadiaCode/Regular/CaskaydiaCoveNerdFontMono-Regular.ttf");
    font-weight: normal;
    font-style: normal;
}

x-row {
    text-rendering: optimizeLegibility;
    font-variant-ligatures: normal;
}
```

Last `x-row` rules, should optimize the font for legibility and enable font ligatures, but the latter do not seem to work at the moment.

Finally locate the "Text font family" field, and prepend to the already present text the `@font-face` `font-family` name that you chose earlier between single quotes. In my example I should use `'Caskaydia Cove NFM'`, so that the full line becomes `'Caskaydia Cove NFM', 'Fira Code', 'Noto Sans Mono'`.

Now the terminal app should use the selected font.

## Other fonts

You could use any font you like, as long as it is hosted somewhere on the Internet, the Terminal app will fetch it and cache it.

In the specific case that you want to use a font hosted on GitHub, like Nerd Fonts, you should be careful to use the link to the Raw GitHub CDN and not the one you get from the GitHub UI, otherwise you will have CORS error. Remember, after all, Chrome OS is a glorified web browser.

The technique that I use to find that link is to use the Network tab on the Chrome Developer Tools (```Ctrl+Shift+i```) and try to download the font from GitHub. Once you identify the request and click on it, the URL used will be the right one. Take a look at the following screenshot:

{% figure
    'dev-tools-network.webp',
    'screenshot of chrome dev tools network tab after downloading a font'
%}

For completeness, you can find all the supported Nerd Fonts, by looking at their GitHub repo, specifically [here](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts).
