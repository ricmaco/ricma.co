---
title: "Introduction to GIMP: GNU Image Manipulator Program"
date: 2016-10-22
tags:
    - 2016
    - talk
    - gimp
    - photo editing
    - image editor
    - linuxday
    - italian
subtitle: A simple course on the basics of GIMP, the libre image manipulator
---
{% figure
    'gimp.webp',
    'gimp logo',
    'left'
%}

Sometimes you just have to resize an image. Sometimes you want to edit a
couple of images. Sometimes you have to do a complete overhaul of a ton
of images.

Now, let me tell you the routine you have to follow; **have** Windows™,
go to your favorite graphic editor, Adobe, website and buy or "buy" a
copy of your favourite image editor, Photoshop.

But, let's say that the PC you have at hand is not capable of Windows,
it could have Linux or, eventually, Mac OS X. Or say that you don't own
the necessary amount of gold coins to buy (and not "buy") the upsaid
software.

A simple, easy and straightforward solution is to search for another
program. An example is GIMP, and to be honest it is **the** example of a
libre/open source solution. It can work on Windows, Linux and Mac OS X
and can handle the majority of use cases of the upcited Photoshop.

For this purpose, I proposed a workshop where I show the basics of the
GIMP, divided in fived practical use cases.

{% iframe '/raw/intro-gimp/presentation.html#1' %}

You can also view the presentation fullscreen [here](/raw/intro-gimp/presentation.html).

If you are interested, you can also download a
[zip](/files/linuxday/2016/intro_gimp/introduzione_gimp.zip) containing
presentation and examples.
