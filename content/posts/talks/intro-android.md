---
title: Introduction to Android programming
date: 2016-09-22
tags:
    - 2016
    - talk
    - android
    - android studio
    - programming
    - italian
subtitle: A crash course on Android and Android Studio, for people who already know Java, with simple but useful examples
---
{% figure
    'android.webp',
    'android logo',
    'right'
%}

A recently but estabilished new entry into the computer and technology
world is mobile computing.

Having calculum resources, together with internet access and a plethora
of software to use at hand is not only comfortable, but is becoming more
and more essential to our everyday life.

The two major platform that have spread in this latest year are Apple's
iOS and Google's Android. Being a Linux enthusiast, for me Android is
the way to go.

Together with using this system I learned through the years to make some
little and useful nice applications, or better called "apps". Since I
learned the lesson "mobile programming is radically different from
traditional programming", I decided to give a small talk to help whoever
wanted to approach this world.

For the sake of completeness I put together three really simple, but
extremely useful in the real case examples.

{% iframe '/raw/intro-android/presentation.html#1' %}

You can also view the talk fullscreen [here](/raw/intro-android/presentation.html).

If you are interested you can also download a
[zip](/files/linuxvar/intro_android/introduzione_android.zip) containing the full presentation.
