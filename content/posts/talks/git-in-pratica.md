---
title: Git in pratica
date: 2017-02-16
tags:
    - 2017
    - talk
    - git
    - programming
    - streaming
    - italian
subtitle: Video tutorial, with presentation on everyday git use and commands
---
{% figure
    'git-in-pratica.webp',
    'git in pratica logo',
    'background'
%}

I already presented on this blog [git](https://git-scm.com/), the SCM
(**S**oftware, **C**onfiguration, **M**anager) of choiche. Last time,
I made a simple introdutory tutorial on the system, showing its
potential, but I didn't dwell enough on the everyday use. I decided to
organize another talk in which all day use and commands were the center
of the conversation.

# Video recap

This time, I chose to record myself talking, instead of only providing
presentation material. So, here it is, the video recap of the evening.

{% youtube 'h2t1opDnd38' %}

# Presentation

The presentation being shown in the video is provided below.

{% iframe '/raw/git-in-pratica/presentation.html#1' %}

Download the
[presentation](/files/linuxvar/git_in_pratica/git_in_pratica.zip) or
[watch it fullscreen](/raw/git-in-pratica/presentation.html).

# Useful information

The shell used during the video stream is the [fish
shell](https://fishshell.com/).

If you desire an informative prompt about the situation of the git
repository in the current directory, you have a couple of viable
options.

  - Bash
      - [git-prompt.sh](https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh)
      - [bash-git-prompt](https://github.com/magicmonty/bash-git-prompt)
  - Zsh
      - [zsh-git-prompt](https://github.com/olivierverdier/zsh-git-prompt)
      - [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
      - [git-prompt.sh](https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh)
  - Fish
      - [\_\_fish\_git\_prompt.fish](https://github.com/fish-shell/fish-shell/blob/master/share/functions/__fish_git_prompt.fish)
      - [oh-my-fish](https://github.com/oh-my-fish/oh-my-fish)

If you want to have the alias `hist`, in order to use the command `git
hist` as in the video, you should download the file
[gitconfig](/files/linuxvar/git_in_pratica/gitconfig) and copy it at
`$HOME/.gitconfig`.
