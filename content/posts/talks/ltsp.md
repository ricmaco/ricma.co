---
title: "LTSP: Linux Terminal Server Project"
date: 2014-10-30
tags:
    - 2014
    - ltsp
    - terminal server
    - linux
    - italian
subtitle: Some slides about the Linux Terminal Server Project
---
{% figure
    'ltsp.webp',
    'ltsp logo',
    'background'
%}

[LTSP](http://www.ltsp.org/) is a server-client technology built in order to fulfill the computing needs of small officies and educational environments.

The typical system is a dedicated network in which the server is a powerful elaborator and the clients (known as **thin clients**) are computers without rotative parts, such as a hard disk, with only a motherboard, a small amount RAM, a very modest CPU, GPU and I/O peripherals (monitor, keyboard and mouse).

The server provides computation power, network connection and a complete operative system, while clients only draw the graphics and handle I/O.

I made a quick and simple presentation of LTSP features, with a stress on how to build a LTSP system easily.

{% pdf '/files/linuxday/2014/ltsp.pdf' %}

Slides can be downloaded [here](/files/linuxday/2014/ltsp.pdf) in PDF format and [here](/files/linuxday/2014/ltsp.odp) in ODP format.
