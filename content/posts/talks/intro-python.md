---
title: Introduction to Python 3
date: 2016-06-01
tags:
    - 2016
    - talk
    - python
    - programming
    - italian
subtitle: A basic introduction to the Python language version 3
---
{% figure
    'python.webp',
    'python 3 logo',
    'left'
%}

A language that is increasingly becoming more and more famous and prominent, both as a scripting language and a scientifical tool, is [Python](https://www.python.org/).

Python lets newcomers accomodate really quickly and easily. Discover it yourself!

During this talk, I'll provide a small introduction to basic concepts to become a real *pythonista*.

<hr />

{% iframe '/raw/intro-python/presentation.html#1' %}

You can also watch the presentation fullscren [here](/raw/intro-python/presentation.html).

If you are interested you can download a [zip](/files/linuxvar/intro_python/introduzione_python.zip) containing the full presentation.
