---
title: "Instagram: #instaprivacy"
date: 2017-10-28
tags:
    - 2017
    - linuxday
    - talk
    - instagram
    - social network
    - privacy
    - italian
subtitle: Presentation about disclosing delicate informations online
---
{% figure
    'instagram.webp',
    'instaprivacy talk logo',
    'background'
%}

In occasion of the [Linux Day 2017](/posts/tech/linux/linux-day-2017), edition of
the province of Varese, I had the opportunity to give a talk about
privacy. The presentation should have been centered on speaking to young
people.

Differently from previous years, I made something different, more
philosofical and much less practical.

My main theme was Instagram, which is the most used social network
platform by young people, but in any case I was eyeing every major
social network platform. The aim was informing young people about the
dangers of putting their whole life at everyones availability and
ultimately telling them how it is possible for everyone able to exploit
the information they donated *gratis* online and all this at their
expense.

# Presentation

The presentation is in Italian and you can view it below.

{% iframe '/raw/instaprivacy/index.html#1' %}

You can also
[download](/files/linuxday/2017/instaprivacy/instaprivacy.zip) the
presentation or [watch it fullscreen](/raw/instaprivacy/index.html).

Of course, if you have any questions, you can write me using one of the contact badges on the page aside.
