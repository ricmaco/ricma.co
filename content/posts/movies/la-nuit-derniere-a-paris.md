---
title: La Nuit Derniére a Parìs
date: 2015-09-20
tags:
  - nighthawks
  - mafia
  - thriller
  - crime novel
subtitle: A short film produced for a multimedia project exam at university
---
As my last exam for the Digital Communication degree I chose to produce
a short film inspired by the work "*Nighthakws*", by [Edward
Hopper](https://en.wikipedia.org/wiki/Edward_Hopper).

If you want to know more about Hopper's work check out [his
page](https://www.artsy.net/artist/edward-hopper) on
[Artsy](https://www.artsy.net/about), a promising online art gallery.

{%- figure
    'nighthawks.webp',
    'nighthawks painting'
%}

The story is about two mafia bosses (one being a female) and a cop
protagonist who struggles to arrest them. In this journey, he will score
some triumphs and suffer some defeats.

If you're interested,
[here](/files/uni/la-nuit-derniere-a-paris/la-nuit-derniere-a-paris_script.pdf)
a copy of the script may be downloaded.

What remains to be said? Let's watch it.

{% youtube '3djXnzmdaAU' %}
