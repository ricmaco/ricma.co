---
title: "Some short films"
date: 2015-04-18
tags:
  - short movies
  - stop motion
  - chroma key
subtitle: Just a dump of some clips I produced for a multimedia project exam at university
---
Ok, I know, I know... My directing, editing and producing ability is not at its finest, although I'm learning.
It is not my field, though, so I do not expect to improve much.

I had to realize two clips for a university exam.

One using the stop motion technique:

{% youtube 'AKndch7vk-E' %}

And one using a chroma key footage:

{% youtube 'nhOYaf9BSEU' %}

Quite amatorial, right? Well, it's a start.
