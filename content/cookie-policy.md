---
layout: layouts/page.njk
title: Cookie policy
tags: info
---
# {% icon 'frown' %} Cookie policy

As you can probably imagine, this site is hosted on the {% icon 'cookie' %} cloud.

The author posts from Italy, so it has to abide to Italian laws about privacy.

With reference to the legislative measure "[Individuazione delle modalità semplificate per l'informativa e l'acquisizione del consenso per l'uso dei cookie](http://www.garanteprivacy.it/web/guest/home/docweb/-/docweb-display/docweb/3118884)" of May 8, 2014 (published onto the Official Journal no. 126 of June 3, 2014) characteristics and aims of cookies used throughout this site are described specifically and analitically.

## First-part cookies

This website, "**Hi, I'm Riccardo**", from now on only "**site**", does not use any first-part profiling cookies; the only cookie used is considered technical and *ironically*, serves the scope to make the site remember you accepted this cookie policy.

## Third-part cookies

On the other side, third-part cookies, either technical or profiling, may be served on this site (but an attempt is made to keep them at a minimum). If you want to learn more about *their* cookie policy, refer to *their* website.

## Further info

More information on how to protect your online privacy (for European countries) can be found at [Your Online Choices](http://www.youronlinechoices.com/).

Some links to relevant third-party policies are put here for convenience:

- [Google Inc. (Youtube included)](https://policies.google.com/privacy)
- [Prezi Inc.](https://prezi.com/terms-of-use/)
