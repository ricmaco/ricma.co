---
layout: layouts/page.njk
title: Licenses
tags: info
---
# {% icon 'paperclip' %} Licenses

## {% icon 'globe' %} CC BY-NC-SA

This site and the majority of its content are licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/) (or the [full text](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode) of the license, if you prefer).

Just to summarize things up, you are free to *share* and *adapt* this work, as long as you follow these rules:

- you give **credit** for the work I have done and clearly indicate what modification were made;
- you don't use any of this for **commercial purposes**;
- you use the same license to redistribute your derivative work.

## {% icon 'lock' %} Reserved rights

Some rights, however, remain reserved. That is the rights for the site name, logo, favicon and images of the author (wherever found) are all reserved, together with all the material marked with a &copy; symbol.

## {% icon 'type' %} Fonts licenses

Although you can easily find the license of the used fonts on [Google Fonts](https://fonts.google.com), below I reported them.

The fonts used are:

- [Fira Sans](https://fonts.google.com/specimen/Fira+Sans/about), by [Carrois Apostrophe](https://carrois.com/), licensed under the [SIL Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL);
- [Fira Code](https://fonts.google.com/specimen/Fira+Code/about) by [The Mozilla Foundation](https://foundation.mozilla.org/en/), [Telefonica S.A.](https://www.telefonica.com/en/) and [Nikita Prokopov](https://twitter.com/nikitonsky), licensed under the [SIL Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL);
- [Nothing You Could Do](https://fonts.google.com/specimen/Nothing+You+Could+Do/about), by [Kimberly Geswein](https://www.kimberlygeswein.com/), licensed under the [SIL Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL).

## {% icon 'image' %} Images licenses

The rights for the images contained in this site, again unless otherwise specified, belong to their respective copyright owners and are therefore possibly used under "fair use".
