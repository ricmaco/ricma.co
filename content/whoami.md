---
layout: layouts/page.njk
title: Who am I?
tags: info
---
# {% icon 'user' %} Who am I?

{% figure
    '/img/hero.webp',
    "author's ugly face",
    'right'
%}

> “It's so simple to be wise... Just think of something stupid to say and then don't say it„
> 
> <span class="smaller">— Homer Simpson</span>

That, up there, is my favourite quote, from my favourite fictional character.

Oh, hi there! I am <span class="handwriting">Riccardo Macoratti</span> and I greet you as author of these modest pages.

If you are interested, below are a few words on myself.

{% journey %}
    {% journeyitem 'home' %}
        <p><strong>1992</strong></p>
        <p>I was born and raised between the noth-west and nothern-east of Italy &#127470;&#127481;.</p>
    {% endjourneyitem %}
    {% journeyitem 'pen-tool' %}
        <p><strong>Late 2000s</strong></p>
        <p>Attended a modern languages high school, one of the best choices of my life.</p>
        <p>There, I learnt fluent English, current Spanish &#128153; and barely passable German.</p>
    {% endjourneyitem %}
    {% journeyitem 'award' %}
        <p><strong>Early to late 2010s</strong></p>
        <p>Completely switched career and went for a computer science background.</p>
        <p>Attended the <a href="https://unimi.it">Università Statale di Milano</a> and earned two degrees: a bachelors' degree in Computer Science for Digital Communication (such a complex name..) and a masters' degree in Computer Science.</p>
        <p>In those years, I developed a passion for FOSS software, particularly the Linux world &#128039;. I even attended a LUG called <a href="https://www.linuxvar.it">LinuxVar</a>.</p>
    {% endjourneyitem %}
    {% journeyitem 'briefcase' %}
        <p><strong>Late 2010s</strong></p>
        <p>Began working in a software consulting company in Milan as a back-end dev.</p>
        <p>Refined my strictly academic CS background with some on-the-job competence and real-world projects.</p>
        <p>Also discovered that I am unable to stay still too much time in my technical comfort zone: learned the front-end way, some light devops and a bunch of new programming languages.</p>
    {% endjourneyitem %}
    {% journeyitem 'home' %}
        <p><strong>Early 2020s</strong></p>
        <p>Moved near the Swiss &#127464;&#127469; border to pursue an employment opportunity. Again, I think it was the right choice, especially for my professional career.</p>
        <p>In the meantime, I have changed a couple employment experiences, but finally settled for one that apreciates me as a persona and my skills.</p>
    {% endjourneyitem %}
{% endjourney %}

## Hobbies and other stuff

But what do I really like to do in my free and non-free time, you ask?

Well, let's see... First of all, I like **computers**, like a lot. These days I think I lost that big interest for recent hardware, and it shifted to **retrocomputing**.

That goes along with my second all-time passion: **YouTube**. I love to scavenge the platform for hours, watching new content: people restoring old computers, repairing old game console, trafficking with CRT displays, playing old games, et cetera. I could waste many, many time consuming this kind of videos. But I don't watch only stuff regarding computers. Over the years, I enjoyed science videos, documentaries, and in general watching people show their skills in some sort of craft.

Thanks to this attitude, I am very curious and like experimenting. This happened with **DIY** contents... By dint of watching them, I have learned some tips and tricks and I became pretty good a it. For example, I hate throwing things away when they are broken, I always try to tinker and repair them.

Last, but not least, I think I am beginning to build interest for **vanlife** and **outdoor camping** content. I love the freedom given by vanlife, although with its drawbacks, or the feeling of being completely surrounded by nature inside a tent.

## Weren't you a dev?

```js
> console.log('Hello, world!');
```

Yes, I am a software developer or, as I prefer calling my similars, a **software engineer**. And an *eclectic* one.

Most of my work career consisted in using **Java**, but that did not prevent me from using something else, once in a while. Also, that thought prompted me to learn Python, Javascript, C/C++, Go, Bash, et cetera.

For pure fun, I leave below my top 5 speaking of programming languages:
1. Go
2. Javascript
3. C++
4. Python
5. Java

## A worthy conclusion

That's all folks!

Enjoy the little time spent on my humble piece of web. If you want to get in touch with me or maybe report some errors, feel free to [write](mailto:r.macoratti@gmx.co.uk).